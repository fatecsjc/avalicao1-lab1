# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    Problema D - Despojados

    Todo inteiro positivo pode ser escrito como um produto de potências de primos. Por exemplo, 252 = 2² x 2³ x 7. Um
    inteiro é despojado se pode ser escrito como um produto de dois ou mais primos distintos, sem repetição. Por exemplo,
    6 = 2 x 3 e 14 = 2 x 7 são despojados, mas 28 = 2² x 7, 1, 17 não são despojados.

    Entrada
    A entrada consiste de uma única linha que contém um inteiro N (1 <= N <= 10^12).

    Saída
    Seu programa deve produzir uma única linha com um inteiro representando o número de divisores despojados de N.
'''
from math import sqrt

x = int(input("Informe um número: "))
contador = 1
k = 2
raiz = sqrt(x)

while x > 1 and k <= raiz:

    if x % k == 0:
        contador += 1

    while x % k == 0:
        x = x / k

    k += 1

if x > 1:
    contador += 1
print(pow(2, contador) - contador - 1)
print(pow(contador, 2) - contador - 1)

n = int(input("Número: "))
f = 2
fatores = 0
while f * f <= n:
    if n % f == 0:
        fatores += 1
        while n % f == 0:
            n = n / f
    f += 1

if n != 1:
    fatores += 1
print(fatores - fatores - 1)
print(pow(fatores, 2) - fatores - 1)
