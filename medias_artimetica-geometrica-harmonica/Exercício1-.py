# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)

__author__ = 'Robson'

'''
    1 - Resolver: ler do teclado a idade dos alunos de uma turma (N alunos). Ao final, quando se digita idade igual a
    zero, calcular a média aritmética das idades, a média geométrica, a média harmônica e o desvio padrão (população e 
    amostral). Imprimir as médias e o desvio padrão calculados: 
'''
from math import sqrt
n, idade, soma, media_aritmetica, media_geometrica, media_harmonica, variancia = 0, -1, 0, 0, 1, 0, 0
idades = []

while idade != 0:
    idade = int(input("Informe sua idade: "))

    if idade > 0:
        n += 1
        soma += idade
        media_geometrica = media_geometrica * idade
        media_harmonica = (media_harmonica + 1 / idade)
        idades.append(idade)

media_aritmetica = soma / n
media_geometrica = media_geometrica ** (1 / n)
media_harmonica = (n / media_harmonica)

for i in idades:
    variancia += (i - media_aritmetica) ** 2

desvio_padrao_populacional = sqrt(variancia / n)
desvio_padrao_amostral = sqrt(variancia / (n - 1))

print("Média aritmética: %2.2f\tMédia geométrica: %2.2f \tMédia harmônica: %2.2f\tDesvio padrão populacional: %2.2f\tDesvio padrão amostral %2.2f"
      % (soma / n, media_geometrica, media_harmonica, desvio_padrao_populacional, desvio_padrao_amostral))

