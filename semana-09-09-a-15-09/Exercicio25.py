# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    25 - Prepare um algoritmo que calcule o valor de H, sendo que ele é determinado pela série
    H = 1/1 + 3/2 + 5/3 + 7/4 + ... + 99/50.
'''
numerador = 1
denominador = 1
h = 0

while denominador <= 50:
    print(numerador, denominador)
    h = h + float(numerador / denominador)
    numerador = numerador + 2
    denominador += 1

print("Série h: %2.2f" % h)
