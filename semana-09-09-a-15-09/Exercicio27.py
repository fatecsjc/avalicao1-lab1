# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    27 - Escreva um algoritmo que calcule e escreva a soma dos dez primeiros termos da seguinte série:
    2/500 - 5/450 + 2/400 - 5/350 + ...
'''

soma = 0
numerador = 2
denominador = 500
termos = 0

while termos <= 10:
    print(numerador, denominador)

    if numerador % 2 == 0:
        soma = soma + float(-(numerador / denominador))
        numerador = (numerador + 3)
    else:
        soma = soma + float(numerador / denominador)
        numerador = (numerador - 3)

    denominador = denominador - 50
    termos += 1

print("Soma dos 10 primeiros termos: %2.3f" % soma)
