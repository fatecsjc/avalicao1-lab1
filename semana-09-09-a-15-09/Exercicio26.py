# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

__author__ = 'Robson'

'''
    26 - Elabore um algoritmo que determine o valor de S, em que:
    S = 1/1 - 2/4 + 3/9 - 4/16 + 5/25 - 6/36 ... -10/100.
'''

s = 0
numerador = 1
denominador = 1

while denominador <= 100:
    print(numerador, denominador)

    if numerador % 2 == 0:
        s = s + float(-(numerador / denominador))
    else:
        s = s + float(numerador / denominador)

    numerador = numerador + 1
    denominador = numerador * numerador

print("Série S: %2.2f" % s)
