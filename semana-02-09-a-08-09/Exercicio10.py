# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)

__author__ = 'Robson'

'''
    10 - A partir da idade informada de uma pessoa, elabore um algoritmo que informe a sua classe eleitoral, 
    sabendo que menores de 16 anos não votam (não votante), que o voto é obrigatório para adultos entre 
    18 e 65 anos (eleitor obrigatório) e que o voto é opcional para eleitores entre 16 e 18, ou maiores de 65
    anos (eleitor facultativo).
'''

idade = int(input("Informe sua idade: "))

if idade < 16:
    print("Não votante.")
if 18 <= idade <= 65:
    print("Eleitor obrigatório.")
if 16 <= idade < 18 or idade > 65:
    print("Eleitor facultativo.")
