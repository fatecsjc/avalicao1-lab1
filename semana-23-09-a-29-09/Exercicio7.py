# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

# Entrega INDIVIDUAL - Exercícios de Fixação

'''
    7. Indique  como  um  troco  deve  ser  dado utilizando-se  um  número  mínimo  de  notas.  Seu algoritmo deve ler o
valor da conta a ser paga e o valor do pagamento efetuado desprezando os centavos. Suponha que as notas para troco
sejam as de 50, 20, 10, 5, 2 e 1 reais, e que nenhuma delas esteja em falta no caixa.
'''

valor_conta = float(input("Entre com o valor da conta: "))
valor_pago = float(input("Valor pago: "))
troco = valor_pago - valor_conta
notas = [50, 20, 10, 5, 2, 1]

x = 0

while troco > 0:
    n = troco / notas[x]
    troco = troco % notas[x]
    if int(n) != 0:
        print("Troco de %d notas de R$ %.2f" % (n, notas[x]))

    x += 1
