# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

# Entrega INDIVIDUAL - Exercícios de Fixação


'''
    2. Faça um programa que leia um nome de usuário e a sua senha e não aceite a senha igual ao nome do usuário,
mostrando uma mensagem de erro e voltando a pedir as informações.
'''

username = "alfa"
password = "alfa"

while username == password:
    username = input("Informe seu nome de usuário: ")
    password = input("Informe sua senha: ")
    if username != password:
        break
    print("O nome de usuário (%s) é o mesmo da senha (%s). Por favor informe nome de usuário diferente da senha." % (username, password))

print("Username %s \tPassword: %s" % (username, password))
