# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

# Entrega INDIVIDUAL - Exercícios de Fixação

'''
    10. Faça um programa que peça um inteiro positivo e o mostre invertido. Ex.: 1234 gera 4321
'''

'''
numero = input("Informe um número: ")
invertido = numero[::-1]
print(int(invertido))
'''

# ou

numero = int(input("Informe um número: "))
unidade = numero // 1 % 10
dezena = numero // 10 % 10
centena = numero // 100 % 10
milhar = numero // 1000 % 10
invertido = str(unidade) + str(dezena) + str(centena) + str(milhar)
print(invertido)

