# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

# Entrega INDIVIDUAL - Exercícios de Fixação

'''
    1.Faça um programa que peça uma nota, entre zero e dez. Mostre uma mensagem caso o valor seja inválido e continue
pedindo até que o usuário informe um valor válido.
'''

nota = float(input("Informe uma nota entre 0 e dez: "))

while nota < 0 or nota > 10:
    nota = float(input("Valor inválido. Informe uma nota entre 0 e dez: "))
    print("Nota: %2.2f" % nota)
