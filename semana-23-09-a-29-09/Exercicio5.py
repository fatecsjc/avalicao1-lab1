# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

# Entrega INDIVIDUAL - Exercícios de Fixação

'''
    5. Dados dois números inteiros positivos, determinar o máximo divisor comum entre eles usando o algoritmo
    de Euclides.
'''

n1 = int(input("Informe o 1° número de valor maior que 0 (n > 0): "))
n2 = int(input("Informe o 2° número de valor maior que 0 (n > 0): "))
print(n1)
print(n2)

mdc = n1

while n1 % mdc != 0 or n2 % mdc != 0:
    mdc -= 1
print("MDC de %d e %d é: %d" % (n1, n2, mdc))
