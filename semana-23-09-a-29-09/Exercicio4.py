# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

# Entrega INDIVIDUAL - Exercícios de Fixação

'''
    4 - A sequência de Fibonacci é a seguinte: 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, ...
    Sua regra de formação é simples: os dois primeiros elementos são 1; a partir de então, cada elemento é a
    soma dos dois anteriores. Faça um algoritmo que leia um número inteiro calcule o seu número de Fibonacci.
    F1 = 1, F2 = 1, F3 = 2, etc.
'''


def calcular_fibonnaci(n):

    if n == 0 or n == 1:
        return n
    else:
        return calcular_fibonnaci(n - 1) + calcular_fibonnaci(n - 2)


print("Fibonnaci de %d é %d" % (9, calcular_fibonnaci(9)))
