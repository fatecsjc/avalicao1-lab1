# Autor: Robson de Sousa
# Laboratório de Desenvolvimento de Banco de Dados I (LAB1)
# Professor: Msc. Lucas Nadalete

# Entrega INDIVIDUAL - Exercícios de Fixação

'''
    9. Dado   um   número   inteiro   positivo,   determine   a   sua   decomposição   em   fatores   primos calculando
também a multiplicidade de cada fator.
'''

x = int(input("Digite um número: "))

divisores = []

divisor = 2

while x > 1:
    if x % divisor == 0:
        x = x / divisor
        divisores.append(divisor)
    else:
        divisor += 1
print("Divisores: %s" % divisores)
